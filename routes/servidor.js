const {Router} = require('express');
const { servidoresGet } = require('../controllers/servidor');

const router = Router();

router.get('/', servidoresGet);

module.exports = router;