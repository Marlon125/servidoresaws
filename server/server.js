const express = require('express');

class Server {
    constructor(){
        this.app = express();
        this.port = 8080;

        this.apiPath = '/api/servidores';

        //RUTAS DE LA APP
        this.routes();
    }

    routes(){
        this.app.use(this.apiPath, require('../routes/servidor'));
    }

    listen() {
        this.app.listen( this.port, () => {
            console.log('Servidor corriendo en puerto', this.port );
        });
    }
}

module.exports = Server;